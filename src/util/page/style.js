import { ref } from 'vue'

export function useFormStyle(w = 130) {
    const labelWidth = ref(w)
    const formItemStyle = ref({
        width: `${labelWidth.value + 350}px`
    })

    return {
        labelWidth,
        formItemStyle
    }
}
