const fs = window.preload.fs;

async function loadProjectInfo(){
    const result = await fs.readFile(`${window.preload.dirname}output/project.json`,'utf8');
    let useResult = eval('(' + result + ')');
    return useResult
}

async function writeProjectJson(params){
    const result = await fs.readFile(`${window.preload.dirname}output/project.json`,'utf8');
    let useResult = eval('(' + result + ')');
    useResult.push(params);
    await fs.writeJsonSync(`${window.preload.dirname}output/project.json`, useResult)
}

async function overloadProjectJson(params){
    await fs.writeJsonSync(`${window.preload.dirname}output/project.json`, params)
}

async function createNewProject(params){
    //创建文件夹
    await fs.mkdirpSync(`${window.preload.dirname}output/${params.id}`);
    //在文件夹下创建json文件夹
    await fs.mkdirpSync(`${window.preload.dirname}output/${params.id}/json`);
    //在文件夹下创建zip文件夹
    await fs.mkdirpSync(`${window.preload.dirname}output/${params.id}/zip`);
    //将template下的project文件，复制到此项目下，
    await fs.copySync(`${window.preload.dirname}output/template/project`,`${window.preload.dirname}output/${params.id}/project`)
    //将template下的router文件，复制到此项目下，
    await fs.copySync(`${window.preload.dirname}output/template/router`,`${window.preload.dirname}output/${params.id}/router`)
}

async function loadProjectNowInfo(){
    const result = await fs.readFile(`${window.preload.dirname}output/nowInfo.json`,'utf8');
    let useResult = eval('(' + result + ')');
    return useResult
}

async function writeProjectNowInfo(params,type){
    const result = await fs.readFile(`${window.preload.dirname}output/nowInfo.json`,'utf8');
    let useResult = eval('(' + result + ')');
    if(type == 'isPreview'){
        useResult.isPreview = params;
    }else if(type == 'nowProject'){
        useResult.nowProject = params;
    }
    await fs.writeJsonSync(`${window.preload.dirname}output/nowInfo.json`, useResult)
}

async function deleteProject(item){
    await fs.removeSync(`${window.preload.dirname}output/${item.id}`);
}


export{
    loadProjectInfo,
    writeProjectJson,
    createNewProject,
    loadProjectNowInfo,
    writeProjectNowInfo,
    deleteProject,
    overloadProjectJson
}