import { defineStore } from 'pinia'

const userStore = defineStore('user', {
    state: () => ({
        info: {}
    }),
    actions: {
        logout(params) {
            this.info = {}
        },
        saveUserInfo(params){
            this.info = {}
        }
    },
    persist: true,
})

export default userStore
