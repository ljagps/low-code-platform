# EDesign 低代码平台

- [视频演示Demo](https://www.zhihu.com/zvideo/1599717373736357888)
- 针对搭建“web后台管理系统”场景的低代码平台，使用Electron搭建的桌面端软件。
- mac安装包：https://pan.baidu.com/s/1tZ8b3mVc9epIkQ64B85hZQ  密码: 5ljo
- 参考链接，[前端知识库：低代码平台](https://www.html5iq.com/6115c37f3a5920292e6f9545.html)

### 环境要求
- Node.js 推荐使用 v16 以上版本
- [下载Node安装包](https://nodejs.org/zh-cn/download/)

### 项目命令

```bash
# 开发启动
npm run dev
# npm 方式：安装 npm 包
npm install
```
```bash
#先打h5包
npm run build 或 npm run build:mock(打包后仍然使用mock)
#打桌面端包
npm run make
```

# 技术使用

- electron、electron-forge
- vue3
- vite
- vue-router
- pinia
- mitt
- element-plus
- swig
- mock.js
- less


