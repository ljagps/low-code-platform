# EDesign Pro

- 一款基于 vue3.0 和 vite 等生态的纯净后台管理系统脚手架
- [项目预览](https://hanxuming.gitee.io/vite-base-pc/)
- 参考链接，[前端知识库：后台管理系统](https://www.html5iq.com/60311cc33a5920292e6f94e1.html)

### 环境要求
- Node.js 版本大于等于 14.13.0 或 12.20.0（ [原因](http://nodejs.cn/api/packages.html#exports) ）
- [下载Node安装包](https://nodejs.org/zh-cn/download/)

### Github加速工具

- [dev-Sidecar](https://gitee.com/docmirror/dev-sidecar)，window、mac使用后很有效，mac上如果最新版本一直自动退出，建议使用`1.7.2`版本。 

### 项目命令

```bash
# 启动
npm run dev
# npm 方式：安装 npm 包
npm install
# 打包
npm run build
# 打包后也需要 mock 生效
npm run build:mock
# 打包后预览打包
npm run serve
```

# vue3.0 生态基础

### [vue3.0](https://v3.cn.vuejs.org/)

- 组合式（Composition Api） `init_setup.js`
- 选项式 Api `init_opt.js`
- 自定义指令 `src/plugins/directives`
- 公用方法 `src/util`，使用 `this.$util.xxx`
- 引入公共变量 `/config/setting.js`，使用 `this.$setting.xxx`
- 组件 `src/components`
    - 类Loading，函数式组件
    - 全局引入组件
    - 一般组件

### [vite](https://cn.vitejs.dev/)

- [vite](https://github.com/vitejs/vite)
- [@vitejs/plugin-legacy](https://www.npmjs.com/package/@vitejs/plugin-legacy)
- [@vitejs/plugin-vue](https://www.npmjs.com/package/@vitejs/plugin-vue)
- [vite-plugin-mock：模拟数据](https://www.npmjs.com/package/vite-plugin-mock)
- [vite-plugin-pages：自动生成页面路径](https://www.npmjs.com/package/vite-plugin-pages)
- [vite-plugin-svg-icons：使用 Svg 图标](https://github.com/vbenjs/vite-plugin-svg-icons)

### [vue-router4.0](https://router.vuejs.org/zh/)

- 嵌套路由
- 导航守卫
- 路由懒加载
- 路由元信息等

### [axios](https://axios-http.com/zh/)

- axios 实例
- post请求
- get请求
- 上传文件
- 拦截器

### [pinia](https://pinia.vuejs.org/)

- [pinia-plugin-persistedstate：防止页面刷新时vuex数据丢失](https://www.npmjs.com/package/pinia-plugin-persistedstate)

### [element plus](https://element-plus.gitee.io/zh-CN/)
### [Mock数据](https://www.npmjs.com/package/vite-plugin-mock)
### 样式

- 重置样式
- 全局公用样式 `src/style/common.less`

### [mitt事务总线](https://www.npmjs.com/package/mitt)

- `this.$mitt.emit`
- `this.$mitt.on`
- `this.$mitt.off`

### [Babel兼容](https://www.babeljs.cn/)
### 多环境配置

- 在 `src/config` 下，目前分了三个环境：
    - env.dev.js 开发
    - env.staging.js 测试
    - env.prod.js 生产
- 在 `.vue` 中使用环境变量：`import.meta.env.MODE`
- 在 `.js` 中使用环境变量，使用`src/config/index.js`

### [SVG图标使用](https://github.com/vbenjs/vite-plugin-svg-icons)

- 在 `src/plugins/svg` 下新建 `名字.svg` 文件，直接即可使用：
```base
<svgIcon name="名字"></svgIcon>
```

# 后台管理系统基础能力

### 模版页规划

- 在 `src/layouts` 下 为整个项目的模版页、包括：
    - 侧边栏组件、
    - 头部组件、
    - 面包屑组件
- 在 `src/router/sideBar.js` 文件中使用模版，其余侧边栏相应页面，都为 模版的 `chilren` 子组件

### 路由相关

- 统一路由、菜单、权限、面包屑
- 在 `src/router/routerConfig.js` 下，配置相应路由信息
- 路由例子一：侧边栏一级路由配置
```bash
{
    name:"activity", //路由名称
    path:"/activity",//路由路径，注意 path 路径必须得有，并且不可重复，每一个都不一样
    component:() => import('@/view/customer/index.vue'),//懒加载引入
    meta:{ //路由元信息
        title:"客户管理" //浏览器页签显示名称
    },
    sideBarIconName:"present",//侧边栏图标名称 
    sideBarName:"客户管理",//侧边栏显示名称
    sideBarShow:true,//是否在侧边栏显示
    auth:"admin",//权限，未使用，可自行根据项目去使用
    children:[],//子路由，如果没有子路由，可删除此字段或者为空
}
``` 
- 路由例子二：侧边栏两层路由配置
```bash
{   
    path:"/product",//路由路径，注意 path 路径必须得有，并且不可重复，每一个都不一样
    meta:{ //路由元信息
        title:"产品管理" //浏览器页签显示名称
    },
    sideBarIconName:"location",//侧边栏图标名称
    sideBarName:"一级目录",//侧边栏显示名称
    sideBarShow:true,//是否在侧边栏显示
    auth:"admin",//菜单权限
    secMenu:[{
        name:"second",//如果有name的话，不允许重复
        path:"/product/second",
        component:() => import('@/view/customer/index.vue'),
        meta:{
            title:"二级目录"
        },
        sideBarIconName:"present",//侧边栏图标名称
        sideBarName:"二级目录",//侧边栏显示名称
        sideBarShow:true,//是否在侧边栏显示
        auth:"admin",//菜单权限
        children:[]
    }]
}
```

### [加载进度条nprogress](https://www.npmjs.com/package/nprogress)
### [全屏展示,vue-fullscreen](https://github.com/mirari/vue-fullscreen/tree/v3)

# 重要问题记录

### vite版本与Element-plus

- vite 版本在 `2.6.14` 的时候，Element-plus 不能使用手动按需引入的方式，否则打包时，会卡死。
- 如果采用 `npm` 方式，需要将 vite 升级到 `2.8.0`，同时需要将 `@vitejs/plugin-vue` 升级到`2.2.0`。
- 如果采用 `pnpm` 方式，则只需要将 vite 升级到 `2.8.0`。


