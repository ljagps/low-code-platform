import { createApp } from 'vue'
import App from './App.vue'

const app = createApp(App);
//重置css
import 'normalize.css/normalize.css';
import './style/public.less';

// router路由
import router from "@/router";
app.use(router);

// pinia
import store from "@/store";
app.use(store);

// mitt事务总线
import mitt from '@/plugins/mitt.js'
app.config.globalProperties.$mitt = mitt;

//引入自定义指令
import directives from './plugins/directives/index.js';
app.use(directives)

///引入公有方法，使用 this.$util.xxx
import * as methods from './util/index.js';
Object.keys(methods).forEach(k => {
    app.config.globalProperties.$util = methods[k]
});

//引入公共变量，使用 this.$setting.xxx
import setting from './config/setting.js';
app.config.globalProperties.$setting = setting;

//引入 svg icon全局图标
import * as ElIconModules from './plugins/icons.js';
Object.keys(ElIconModules).forEach(iconName => {
    app.component(iconName,ElIconModules[iconName])
});

//引入全局通用组件
import myComponents from './components/overall/overall.js';
Object.keys(myComponents).forEach(k => app.component(k, myComponents[k]));

//手动引入 element-plus组件：message,loading组件
import { useElementPlus } from './plugins/element-plus'
useElementPlus(app)
//自动导入时候使用
import 'element-plus/dist/index.css'
//引入svg
import 'virtual:svg-icons-register'
//引入全屏组件
import VueFullscreen from 'vue-fullscreen'
app.use(VueFullscreen)

//引入函数式组件：loading
import Loading from './components/functionCom/loading/loading.js';
app.use(Loading);
//引入函数式组件：Toast
import Toast from './components/functionCom/toast/toast.js';
app.use(Toast);

app.mount('#app');
