import sideBarPage from './sideBar'

// 单个页面显示：登陆页面、错误页面等
const fameOutPage = [
    {
        path: '/login',
        name: 'login',
        meta:{
            auth: false,
            title:"登陆"
        },
        component: () => import('@/view/login.vue')
    }
]

// 重新组织后导出
export default [
    ...sideBarPage,
    ...fameOutPage
]
