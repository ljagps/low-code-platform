import { createVNode, render } from 'vue';
//还有问题
// 引入对应的组件
import loading from "./loading.vue";

// 2. 准备一个DOM容器
const Profile = document.createElement('div')
document.body.appendChild(Profile)

class Loading {
    show(show){
        const vnode = createVNode(loading, { show:true })
        // 4. 把虚拟dom渲染到div
        render(vnode, Profile)
    }
    hide(){
        render(null, Profile)
    }
}
// 定义对象:开发插件对象
const LoadPlugin = {
    // 插件包含install方法
    install(app,options){
        // 添加实例方法，挂载至Vue原型
        app.config.globalProperties.$Loading = new Loading();
    }
};

// 导出对象
export default LoadPlugin;
