//全局通用组件
//示例
import btn from './btn.vue';
import svgIcon from './svgIcon.vue';

export default {
    btn,
    svgIcon
}
