function treeToArray(tree) {
  let res = []
  for (const item of tree) {
    const { secMenu, ...i } = item
    if (secMenu && secMenu.length) {
      res = res.concat(treeToArray(secMenu))
    }
    res.push(i)
  }
  return res
}
function arrayToTree(items) {
  let res = [] // 存放结果集
  let map = {}
  // 判断对象是否有某个属性
  let getHasOwnProperty = (obj, property) => Object.prototype.hasOwnProperty.call(obj, property)

  // 边做map存储，边找对应关系
  for (const i of items) {
      map[i.id] = {
          ...i,
          children: getHasOwnProperty(map, i.id) ? map[i.id].children : []
      }
      const newItem = map[i.id]
      if (i.pid === 0) {
          res.push(newItem)
      } else {
          if (!getHasOwnProperty(map, i.pid)) {
              map[i.pid] = {
                  children: []
              }
          }
          map[i.pid].children.push(newItem)
      }
  }
  return res
}

const filterTreeByFunc = (tree, func = (item) => {return item.sideBarShow === true}) => {
  if (!Array.isArray(tree) || tree.length === 0) {
      return []
  }
  return tree.filter(item => {
      item.secMenu = item.secMenu && filterTreeByFunc(item.secMenu, func)
      return func(item) || (item.secMenu && item.secMenu.length)
  })
}

// 查找父节点
function findParent(path, list = [], result = []) {
  for (let i = 0; i < list.length; i += 1) {
    const item = list[i]
    // 找到目标
    if (item.path === path) {
      // 加入到结果中
      result.push(item)
      // 因为可能在第一层就找到了结果，直接返回当前结果
      if (result.length === 1) return result
      return true
    }
    // 如果存在下级节点，则继续遍历
    if (item.secMenu) {
      // 预设本次是需要的节点并加入到最终结果result中
      result.push(item)
      const find = findParent(path, item.secMenu, result)
      // 如果不是false则表示找到了，直接return，结束递归
      if (find) {
        return result
      }
      // 到这里，意味着本次并不是需要的节点，则在result中移除
      result.pop()
    }
  }
  // 如果都走到这儿了，也就是本轮遍历children没找到，将此次标记为false
  return false
}

function onlyPageObj(arr){
	let pageObj = {};
	arr.forEach(item => {
		if(item.name){
			pageObj[item.name] = item;
		}
	});
	return pageObj;
}

function arr_findItem(arr, path) {
	let result = {};
	for (let i = 0; i < arr.length; i++) {
		let arrItem = arr[i];
		if (arrItem.path == path) {
			result = arrItem;
			break;
		}
	}
	return result;
}

export {
  treeToArray,
  arrayToTree,
  filterTreeByFunc,
  findParent,
  onlyPageObj,
  arr_findItem
}