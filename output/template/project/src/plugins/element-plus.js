import {
    ElLoading,
    ElMessage,
} from 'element-plus'

export function useElementPlus(app) {
    app.use(ElLoading)
    app.use(ElMessage)
}