//  mockProdServer.js，用于生产需要 Mock 数据时候使用
import { createProdMockServer } from 'vite-plugin-mock/es/createProdMockServer'

// 逐一导入您的mock.ts文件
// 如果使用vite.mock.config.ts，只需直接导入文件
// 可以使用 import.meta.glob功能来进行全部导入
import custom from '../mock/custom.js'

export function setupProdMockServer() {
  createProdMockServer([...custom])
}