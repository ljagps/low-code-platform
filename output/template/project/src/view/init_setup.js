//引入vue相关方法
import {
    ref,
    reactive, 
    toRefs,
    computed,
    watch,
    defineProps,
    defineEmits,
    nextTick,
    onMounted
} from 'vue';
//引入路由
import { useRoute, useRouter } from 'vue-router';
//引入vuex
import { useStore } from 'vuex';

export default function init_setup(){
    const init_index = ref('hanxuming');
    const init_onMounted = onMounted(() => {
        console.log(init_index.value);
		console.log('init，mounted');
	})
    return {
        init_index,
        init_onMounted
    }
}
  